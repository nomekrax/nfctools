FROM debian:stretch

MAINTAINER Nomekrax & Freebien

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
      build-essential \
      pkg-config \
      git \
      automake \ 
      libnfc5 \
      libnfc-bin \
      libnfc-dev \
      hexedit \
      python-libusb1 \
      python3-libusb1

WORKDIR /tmp
RUN git clone https://github.com/nfc-tools/mfoc
RUN cd mfoc;\
    autoreconf -vis; \
    ./configure; \
    make && make install

WORKDIR /tmp
RUN git clone https://github.com/nfc-tools/mfcuk
RUN cd mfcuk; \
    autoreconf -vis; \
    ./configure; \
    make && make install

WORKDIR /home
CMD /bin/bash
